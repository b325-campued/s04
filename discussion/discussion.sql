-- Add 5 artist
INSERT INTO artists (name) VALUES ("Taylor Swift");
INSERT INTO artists (name) VALUES ("Lady Gaga");
INSERT INTO artists (name) VALUES ("Justin Bieber");
INSERT INTO artists (name) VALUES ("Ariana Grande");
INSERT INTO artists (name) VALUES ("Bruno Mars");

-- Add albums and songs
-- Taylor Swift
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Fearless", "2008-1-1", 3);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Red", "2012-1-1", 3);

INSERT INTO songs (song_title, song_length, song_genre, album_id) VALUES ("Fearless", 246, "Pop rock", 3);
INSERT INTO songs (song_title, song_length, song_genre, album_id) VALUES ("Love Story", 213, "Country pop", 3);

INSERT INTO songs (song_title, song_length, song_genre, album_id) VALUES ("State of Grace", 253, "Rock, alternative rock, arena rock", 4);
INSERT INTO songs (song_title, song_length, song_genre, album_id) VALUES ("Red", 204, "Country", 4);

-- Laby Gaga
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("A Star Is Born", "2018-1-1", 4);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Born This Way", "2011-1-1", 4);

INSERT INTO songs (song_title, song_length, song_genre, album_id) VALUES ("Black Eyes", 151, "Rock and roll", 5);
INSERT INTO songs (song_title, song_length, song_genre, album_id) VALUES ("Shallow", 201, "Country, rock, folk rock", 5);


-- EXPORTING
-- mysqldump -u root -p database_name > file_name.sql

-- IMPORTING
-- Go to directory of your exported file
-- mysql -u root -p database_name_to_export_db < file_name_db_exported.sql


-- ADVANCED Selects;

-- Exclude record
SELECT * FROM songs WHERE id != 11;

-- Choose columns to show
SELECT song_name, genre FROM songs WHERE id != 11;

-- Greater than or equal operator
-- Less than or equal operator
SELECT * FROM songs WHERE id >= 11;
SELECT * FROM songs WHERE id <= 11;

-- What if I want those records with id from 5 to 11
SELECT * FROM songs WHERE id >= 5 AND id <= 11;

SELECT *
FROM songs
WHERE id BETWEEN 5 AND 11;

-- GET specific IDs
SELECT * FROM songs WHERE id = 11;

-- OR Operator
SELECT * FROM songs WHERE id = 1 OR id = 11 OR id = 5;
SELECT * FROM songs WHERE id IN (1, 11, 5);

--Find Partial Matches:
-- End in a
SELECT * FROM songs WHERE song_name LIKE "%a";
-- Start with st
SELECT * FROM songs WHERE song_name LIKE "st%";
-- All with a in middle
SELECT * FROM songs WHERE song_name LIKE "%a%";


SELECT * FROM songs WHERE song_name LIKE "%e"; 

SELECT * FROM songs WHERE song_name LIKE "10_"; 

-- SORT
SELECT * FROM songs ORDER BY song_name ASC; 
SELECT * FROM songs ORDER BY song_name DESC; 


-- DISTINCT Records (No duplicates only data distinct in that column)
SELECT DISTINCT genre FROM	songs;

-- TABLE JOINS 
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id;

-- JOIN albums table and songs table
SELECT * FROM albums JOIN songs ON albums.id = songs.album_id;
SELECT albums.album_title, songs.song_name FROM albums JOIN songs ON albums.id = songs.album_id;

SELECT * FROM artists 
	JOIN albums ON artists.id = albums.artist_id 
	JOIN songs ON albums.id = songs.album_id;

SELECT artists.id, albums.album_title, songs.song_name FROM artists 
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;
SELECT artists.name, albums.album_title, songs.song_name FROM artists 
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;
